/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 *  open the template in the editor.
 */
package com.arbolesprog3.controlador;

import com.arbolesprog3.excepcion.CelularExcepcion;
import com.arbolesprog3.modelo.ArbolABB;
import com.arbolesprog3.modelo.ArbolBatABB;
import com.arbolesprog3.modelo.ArbolBatN;
import com.arbolesprog3.modelo.ArbolN;
import com.arbolesprog3.modelo.Celular;
import com.arbolesprog3.modelo.Marca;
import com.arbolesprog3.modelo.NodoABB;
import com.arbolesprog3.modelo.NodoBatABB;
import com.arbolesprog3.modelo.NodoBatN;
import com.arbolesprog3.modelo.NodoN;
import com.arbolesprog3.modelo.Operador;
import com.arbolesprog3.modelo.TipoBarco;
import com.arbolesprog3.utilidades.FacesUtils;
import com.arbolesprog3.utilidades.JsfUtil;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import org.primefaces.model.diagram.Connection;
import org.primefaces.model.diagram.DefaultDiagramModel;
import org.primefaces.model.diagram.DiagramModel;
import org.primefaces.model.diagram.Element;
import org.primefaces.model.diagram.connector.StraightConnector;
import org.primefaces.model.diagram.endpoint.DotEndPoint;
import org.primefaces.model.diagram.endpoint.EndPoint;
import org.primefaces.model.diagram.endpoint.EndPointAnchor;

/**
 *
 * @author Admin
 */
@Named(value = "controladorBatalla")
@ApplicationScoped
public class ControladorBatalla implements Serializable {
    
 
    private ArbolBatABB arbol= new ArbolBatABB();
    private TipoBarco barco= new TipoBarco();   
    
    private ArbolBatN arbolN = new ArbolBatN();
    private ArbolBatN arbolNN = new ArbolBatN();
    private DefaultDiagramModel model;
    private boolean verRegistrar = false;
    private TipoBarco tbarco= new TipoBarco();
    private String referencia;
    
    
    

    /**
     * Creates a new instance of ControladorBatalla
     */
    public ControladorBatalla() {
    }
    
    

    public boolean isVerRegistrar() {
        return verRegistrar;
    }

    public void setVerRegistrar(boolean verRegistrar) {
        this.verRegistrar = verRegistrar;
    }

    public ArbolBatABB getArbol() {
        return arbol;
    }

    public void setArbol(ArbolBatABB arbol) {
        this.arbol = arbol;
    }

    public TipoBarco getBarco() {
        return barco;
    }

    public void setBarco(TipoBarco barco) {
        this.barco = barco;
    }

    public TipoBarco getTbarco() {
        return tbarco;
    }

    public void setTbarco(TipoBarco tbarco) {
        this.tbarco = tbarco;
    }

    public String getReferencia() {
        return referencia;
    }

    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }

    public ArbolBatN getArboln() {
        return arbolN;
    }

    public void setArboln(ArbolBatN arboln) {
        this.arbolN = arboln;
    }

    public ArbolBatN getArbolN() {
        return arbolN;
    }

    public void setArbolN(ArbolBatN arbolN) {
        this.arbolN = arbolN;
    }

    public ArbolBatN getArbolNN() {
        return arbolNN;
    }

    public void setArbolNN(ArbolBatN arbolNN) {
        this.arbolNN = arbolNN;
    }
    
    

   
    
    
    @PostConstruct
    private void iniciar() {
        //arbolN.adicionarNodoNBarco(new TipoBarco("yo", "0", "0"), null);
        //arbol.adicionarTipoBarco(new TipoBarco("DESTRUCTOR", "1", "1"));
        //arbol.adicionarTipoBarco(new TipoBarco("NAVIO", "2", "2"));
        //arbol.adicionarTipoBarco(new TipoBarco("BARCAZA", "3", "2"));
        //arbol.adicionarTipoBarco(new TipoBarco("PORTAVIONES", "4", "4"));
        /*arbol.adicionarTipoBarco(new TipoBarco("SUBMARINO", "5", "5"));
        arbol.adicionarTipoBarco(new TipoBarco("FLOTANTE", "6", "6"));*/
       
        //cargarArbol();
        
        //pintarArbol();
        
        
        
    }
    
    /* //PINTAR ARBOL ABB
    public void pintarArbol()
    {       
        model = new DefaultDiagramModel();
        model.setMaxConnections(-1);
        model.setConnectionsDetachable(false);
        StraightConnector connector = new StraightConnector();
        connector.setPaintStyle("{strokeStyle:'#404a4e', lineWidth:2}");
        connector.setHoverPaintStyle("{strokeStyle:'#20282b'}");
        model.setDefaultConnector(connector);
        pintarArbol(arbol.getRaiz(), model, null, 30, 0);        
    }
    
    private void pintarArbol(NodoBatABB reco,DefaultDiagramModel model, 
            Element padre, int x, int y) {
               
        if (reco != null) {
            Element elementHijo = new Element(reco.getBarco());
            
            elementHijo.setX(String.valueOf(x)+"em");
            elementHijo.setY(String.valueOf(y)+"em");
            
            if(padre!=null)
            {
                elementHijo.addEndPoint(new DotEndPoint(EndPointAnchor.TOP));
                DotEndPoint conectorPadre=new DotEndPoint(EndPointAnchor.BOTTOM);
                padre.addEndPoint(conectorPadre);                
                model.connect(new Connection(conectorPadre, elementHijo.getEndPoints().get(0)));        
                
            }    
            
            model.addElement(elementHijo);
            
            pintarArbol(reco.getIzquierda(),model, elementHijo,x-5,y+8);
            pintarArbol(reco.getDerecha(),model,elementHijo,x+5, y+8);
        }
    }
    
    private EndPoint createEndPoint(EndPointAnchor anchor) {
        DotEndPoint endPoint = new DotEndPoint(anchor);
        endPoint.setStyle("{fillStyle:'#404a4e'}");
        endPoint.setHoverStyle("{fillStyle:'#20282b'}");

        return endPoint;
    }

    public DiagramModel getModel() {
        return model;
    }*/
    
    public void habilitarRegistrar() {
        verRegistrar = true;
        tbarco = new TipoBarco();
    }
    
    public void deshabilitarRegistrar() {
        verRegistrar = false;
    }
    
    public void guardarBarco()
    {
        
            arbol.adicionarTipoBarco(barco);            
            barco = new TipoBarco();
            verRegistrar=false;
            /*cargarArbol();
            pintarArbol();*/
       
    }
    
    public void crearFlota()
    {
          /*arbol.adicionarTipoBarco(barco);            
            barco = new TipoBarco();
            verRegistrar=false;*/
            
            cargarArbol();
            pintarArbol();
       
    }
    
    
    
    /*public void pintarArbol()
    {
        
        model = new DefaultDiagramModel();
        model.setMaxConnections(-1);
        model.setConnectionsDetachable(false);
        StraightConnector connector = new StraightConnector();
        connector.setPaintStyle("{strokeStyle:'#404a4e', lineWidth:2}");
        connector.setHoverPaintStyle("{strokeStyle:'#20282b'}");
        model.setDefaultConnector(connector);
        pintarArbol(arbolN.getRaiz(), model, null, 30, 0);
        
    }
  
    private void pintarArbol(NodoBatN reco,DefaultDiagramModel model, Element padre, int x, int y) {
               
        if (reco != null) {
            Element elementHijo = new Element(reco.getBarco());
            
            elementHijo.setX(String.valueOf(x)+"em");
            elementHijo.setY(String.valueOf(y)+"em");
            elementHijo.setId(reco.getBarco().getNroCasillas());
            
            if(padre!=null)
            {
                elementHijo.addEndPoint(new DotEndPoint(EndPointAnchor.TOP));
                DotEndPoint conectorPadre=new DotEndPoint(EndPointAnchor.BOTTOM);
                padre.addEndPoint(conectorPadre);                
                model.connect(new Connection(conectorPadre, elementHijo.getEndPoints().get(0)));        
                
            }    
            
            model.addElement(elementHijo);
            for(NodoBatN hijo: reco.getHijos())
            {
                pintarArbol(hijo,model, elementHijo,x-10,y+5);
                x += 10;                
            }
        }
    }
    
    private EndPoint createEndPoint(EndPointAnchor anchor) {
        DotEndPoint endPoint = new DotEndPoint(anchor);
        endPoint.setStyle("{fillStyle:'#404a4e'}");
        endPoint.setHoverStyle("{fillStyle:'#20282b'}");

        return endPoint;
    }*/
    

    public DiagramModel getModel() {
        return model;
    }
    
    
    
    public void cargarArbol()
    {
             //Algoritmo
            arbolN.setRaiz(null); 
            arbolN.adicionarNodoNBarco(new TipoBarco("yo", "0", "0"), null);
             
             List<TipoBarco> padres= new ArrayList<TipoBarco>();                          
             padres.add(arbolN.getRaiz().getBarco());
             adicionarPreOrdenBarco(arbol.getRaiz(),padres,0);
             padres = null;
             
            System.out.println("Cantidad: "+getArbol().getCantidadNodos());
//             for(Infante inf: contAbb.getArbol().recorrerPreOrden())
//             {
//                 System.out.println(inf.getNombre()+"\n");
//             }
            
    }
    
    
    
    private void adicionarPreOrdenBarco (NodoBatABB reco, List<TipoBarco> padres, int contizq) {
        if (reco != null) {
            List<TipoBarco> padresNuevos= new ArrayList<>();
            int contPapas=0;
            for(byte i=0;i< Integer.parseInt(reco.getBarco().getCantidadJuego());i++)
            {    
                TipoBarco barcoNuevo = new TipoBarco(reco.getBarco().getNombre(), reco.getBarco().getNroCasillas(), 
                        reco.getBarco().getCantidadJuego());
                
                barcoNuevo.setCodigo(++contizq);
                if(contPapas>=padres.size())
                {
                    contPapas=0;
                }
                arbolN.adicionarNodoxCodigo(barcoNuevo, padres.get(contPapas));
                //arbolN.adicionarNodoNBarco(barcoNuevo,padres.get(contPapas).getNroCasillas());
                padresNuevos.add(barcoNuevo);
                contPapas++;
            }
            adicionarPreOrdenBarco(reco.getIzquierda(),padresNuevos, contizq);
            contizq=contizq+getArbol().sumarInOrdenRecursivoBarco(reco.getIzquierda());
            adicionarPreOrdenBarco(reco.getDerecha(),padresNuevos, contizq);
             //   contizq=contizq+contder;
            /*if(reco.getDerecha()!=null)
            {
               contder=reco.getDerecha().getDato().getEdad();
            }*/
            
//            if(reco.getIzquierda()!=null)
//            {
//                adicionarPreOrden(reco.getIzquierda(),padresNuevos, contizq,contder);  
//                contizq=contizq+reco.getIzquierda().getDato().getEdad();
//            }
//            adicionarPreOrden(reco.getDerecha(),padresNuevos,contizq, contder);
        }
    }
    
    
    public void pintarArbol()
    {
        
        model = new DefaultDiagramModel();
        model.setMaxConnections(-1);
        model.setConnectionsDetachable(false);
        StraightConnector connector = new StraightConnector();
        connector.setPaintStyle("{strokeStyle:'#404a4e', lineWidth:2}");
        connector.setHoverPaintStyle("{strokeStyle:'#20282b'}");
        model.setDefaultConnector(connector);
        pintarArbol(arbolN.getRaiz(), model, null, 30, 0);
        
    }
    
    
    

    private void pintarArbol(NodoBatN reco,DefaultDiagramModel model, Element padre, int x, int y) {
               
        if (reco != null) {
            Element elementHijo = new Element(reco.getBarco());
            
            elementHijo.setX(String.valueOf(x)+"em");
            elementHijo.setY(String.valueOf(y)+"em");
            //elementHijo.setId(reco.getDato().getNroIdentificacion());
            
            if(padre!=null)
            {
                elementHijo.addEndPoint(new DotEndPoint(EndPointAnchor.TOP));
                DotEndPoint conectorPadre=new DotEndPoint(EndPointAnchor.BOTTOM);
                padre.addEndPoint(conectorPadre);                
                model.connect(new Connection(conectorPadre, elementHijo.getEndPoints().get(0)));        
                
            }    
            
            model.addElement(elementHijo);
            for(NodoBatN hijo: reco.getHijos())
            {
                pintarArbol(hijo,model, elementHijo,x-10,y+5);
                x += 10;                
            }
        }
    }
    
    ///////////////TABLERO
    
    
    /*public String pintarBarcos(int fila, int columna)
    {
        for(Barco miBarquito:listadoBarcos)
        {
            if(miBarquito.validarCoordenada(fila, columna))
            {
               if(miBarquito.getNombre().compareTo("Lancha")==0)
               {
                return "width: 80px; heigth: 80px; background-color: green;";
               }
               else if(miBarquito.getNombre().compareTo("Acorazado")==0)
               {
                   return "width: 80px; heigth: 80px; background-color: blue;";
               }
                   
            }
        }
        return "width: 80px; heigth: 80px;";
    }
    
    
    public boolean validarCoordenadaOcupada(int fila, int columna)
    {
        if(fila==2 && columna ==2)
        {
            return true;
        }    
        return false;
    }        
    
    
    public void disparar(int fila, int columna)
    {
        JsfUtil.addSuccessMessage("Has disparado en la fila: "+fila + " columna: "+columna);
    }
    
    public String obtenerEstilo(int fila, int columna)
    {
        if(fila %2 ==0 && columna %2 ==0)
        {
            return "width: 80px; height: 80px; background-color: blue";
        }    
        
        return "width: 80px; height: 80px;";
    }   
    
    
    
    public boolean verififcarDisparo(int fila, int columna)
    {
        for(Disparo disp: disparosJuga1)
        {
            if(disp.getFila()==fila && disp.getColumna()==columna)
            {
                return true;
            }    
        }        
        return false;
    }
    
    
    public void dispararJug1(int fila, int columna)
    {
        disparosJuga1.add(new Disparo(fila, columna));
    }
    
    */
}
