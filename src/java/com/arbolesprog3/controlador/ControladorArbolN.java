/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arbolesprog3.controlador;

import com.arbolesprog3.excepcion.CelularExcepcion;
import com.arbolesprog3.modelo.ArbolN;
import com.arbolesprog3.modelo.Celular;
import com.arbolesprog3.modelo.Marca;
import com.arbolesprog3.modelo.NodoABB;
import com.arbolesprog3.modelo.NodoN;
import com.arbolesprog3.modelo.Operador;
import com.arbolesprog3.modelo.TipoBarco;
import com.arbolesprog3.utilidades.FacesUtils;
import com.arbolesprog3.utilidades.JsfUtil;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import org.primefaces.model.diagram.Connection;
import org.primefaces.model.diagram.DefaultDiagramModel;
import org.primefaces.model.diagram.DiagramModel;
import org.primefaces.model.diagram.Element;
import org.primefaces.model.diagram.connector.StraightConnector;
import org.primefaces.model.diagram.endpoint.DotEndPoint;
import org.primefaces.model.diagram.endpoint.EndPoint;
import org.primefaces.model.diagram.endpoint.EndPointAnchor;

/**
 *
 * @author Admin
 */
@Named(value = "controladorArbolN")
@SessionScoped
public class ControladorArbolN implements Serializable {
    
    private DefaultDiagramModel model;
    private ArbolN arbol= new ArbolN();
    private List<Marca> marcas;
    private List<Operador> operadores;
    private boolean verRegistrar = false;
    private Celular celular= new Celular();
    private String referencia;
    private TipoBarco tipobarco = new TipoBarco();
    
    private ControladorBatalla contAbb=(ControladorBatalla) FacesUtils.
                     getManagedBean("controladorBatalla");

    /**
     * Creates a new instance of ControladorArbolN
     */
    public ControladorArbolN() {
    }

    public String getReferencia() {
        return referencia;
    }

    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }
    
    

    public List<Marca> getMarcas() {
        return marcas;
    }

    public void setMarcas(List<Marca> marcas) {
        this.marcas = marcas;
    }
    
    
    public Celular getCelular() {
        return celular;
    }

    public void setCelular(Celular celular) {
        this.celular = celular;
    }

    public TipoBarco getTipobarco() {
        return tipobarco;
    }

    public void setTipobarco(TipoBarco tipobarco) {
        this.tipobarco = tipobarco;
    }
    
    
    
    public void pintarArbolN()
    {
        
        model = new DefaultDiagramModel();
        model.setMaxConnections(-1);
        model.setConnectionsDetachable(false);
        StraightConnector connector = new StraightConnector();
        connector.setPaintStyle("{strokeStyle:'#404a4e', lineWidth:2}");
        connector.setHoverPaintStyle("{strokeStyle:'#20282b'}");
        model.setDefaultConnector(connector);
        pintarArbolN(arbol.getRaiz(), model, null, 30, 0);
        
    }
  
    private void pintarArbolN(NodoN reco,DefaultDiagramModel model, Element padre, int x, int y) {
               
        if (reco != null) {
            Element elementHijo = new Element(reco.getBarco());
            
            elementHijo.setX(String.valueOf(x)+"em");
            elementHijo.setY(String.valueOf(y)+"em");
            elementHijo.setId(reco.getBarco().getNroCasillas());
            
            if(padre!=null)
            {
                elementHijo.addEndPoint(new DotEndPoint(EndPointAnchor.TOP));
                DotEndPoint conectorPadre=new DotEndPoint(EndPointAnchor.BOTTOM);
                padre.addEndPoint(conectorPadre);                
                model.connect(new Connection(conectorPadre, elementHijo.getEndPoints().get(0)));        
                
            }    
            
            model.addElement(elementHijo);
            for(NodoN hijo: reco.getHijos())
            {
                pintarArbolN(hijo,model, elementHijo,x-10,y+5);
                x += 10;                
            }
        }
    }
    
    private EndPoint createEndPoint(EndPointAnchor anchor) {
        DotEndPoint endPoint = new DotEndPoint(anchor);
        endPoint.setStyle("{fillStyle:'#404a4e'}");
        endPoint.setHoverStyle("{fillStyle:'#20282b'}");

        return endPoint;
    }

    public DiagramModel getModel() {
        return model;
    }
    
    private void llenarMarcas()
    {
        marcas = new ArrayList<>();
        marcas.add(new Marca((short)1, "Huawei"));
        marcas.add(new Marca((short)2, "LG"));
        marcas.add(new Marca((short)3, "Sony"));
        
    }
    
    private void llenarOperadores()
    {
        operadores= new ArrayList<>();
        operadores.add(new Operador("Movistar", "Manizales", "4G"));
        operadores.add(new Operador("Claro", "Pereira", "3G"));
        
    }
    
    @PostConstruct
    private void iniciar(){
        /*llenarMarcas();
        llenarOperadores();
        arbol.adicionarNodoN(new Celular("0", "310", marcas.get(0), operadores.get(0), "negro", 32154), null);
        arbol.adicionarNodoN(new Celular("1", "310", marcas.get(0), operadores.get(0), "negro", 32154), "0");
        arbol.adicionarNodoN(new Celular("2", "310", marcas.get(0), operadores.get(0), "negro", 32154), "0");
        arbol.adicionarNodoN(new Celular("3", "310", marcas.get(0), operadores.get(0), "negro", 32154), "1");
        arbol.adicionarNodoN(new Celular("4", "310", marcas.get(0), operadores.get(0), "negro", 32154), "1");
        arbol.adicionarNodoN(new Celular("5", "310", marcas.get(0), operadores.get(0), "negro", 32154), "2");
        arbol.adicionarNodoN(new Celular("7", "310", marcas.get(0), operadores.get(0), "negro", 32154), "0");
        arbol.adicionarNodoNBarco(new TipoBarco("yo", "0", "0"), null);
        arbol.adicionarNodoNBarco(new TipoBarco("canoa", "2", "2"), "0");
        arbol.adicionarNodoNBarco(new TipoBarco("chalupa", "1", "3"), "0");*/
        cargarArbol();
        pintarArbolN();
        
        
        
    }
    
    public boolean isVerRegistrar() {
        return verRegistrar;
    }

    public void setVerRegistrar(boolean verRegistrar) {
        this.verRegistrar = verRegistrar;
    }

    public void habilitarRegistrar() {
        verRegistrar = true;
        celular = new Celular();
    }
    
    public void deshabilitarRegistrar() {
        verRegistrar = false;
    }
    
    public void guardarCelular()
    {
        
            arbol.adicionarNodoNBarco(tipobarco, referencia);
            tipobarco = new TipoBarco();
            verRegistrar=false;
            pintarArbolN();
        
    }
    
    
    
    public void cargarArbol()
    {
         
            //Algoritmo
            arbol.adicionarNodoNBarco(new TipoBarco("yo", "0", "0"), null);
            
             
            List<TipoBarco> padres= new ArrayList<TipoBarco>();
            padres.add(arbol.getRaiz().getBarco());
         //   adicionarPreOrdenBarco(contAbb.getArbol().getRaiz(),padres,0);
             
            System.out.println("Cantidad: "+contAbb.getArbol().
                     getCantidadNodos());
//             for(Infante inf: contAbb.getArbol().recorrerPreOrden())
//             {
//                 System.out.println(inf.getNombre()+"\n");
//             }
             
    }
    
    
    
    private void adicionarPreOrdenBarco (NodoABB reco, List<TipoBarco> padres, int contizq) {
        if (reco != null) {
            List<TipoBarco> padresNuevos= new ArrayList<>();
            int contPapas=0;
            for(byte i=0;i< Integer.parseInt(reco.getBarco().getNroCasillas());i++)
            {    
                TipoBarco barcoNuevo = new TipoBarco(reco.getBarco().getNombre(), reco.getBarco().getNroCasillas(), 
                        reco.getBarco().getCantidadJuego());
                
                barcoNuevo.setCodigo(++contizq);
                if(contPapas>=padres.size())
                {
                    contPapas=0;
                }
                arbol.adicionarNodoNBarco(barcoNuevo,String.valueOf (padres.get(contPapas)));
                padresNuevos.add(barcoNuevo);
                contPapas++;
            }
            adicionarPreOrdenBarco(reco.getIzquierda(),padresNuevos, contizq);
          //  contizq=contizq+contAbb.getArbol().sumarInOrdenRecursivoBarco(reco.getIzquierda());
            adicionarPreOrdenBarco(reco.getDerecha(),padresNuevos, contizq);
             //   contizq=contizq+contder;
            /*if(reco.getDerecha()!=null)
            {
               contder=reco.getDerecha().getDato().getEdad();
            }*/
            
//            if(reco.getIzquierda()!=null)
//            {
//                adicionarPreOrden(reco.getIzquierda(),padresNuevos, contizq,contder);  
//                contizq=contizq+reco.getIzquierda().getDato().getEdad();
//            }
//            adicionarPreOrden(reco.getDerecha(),padresNuevos,contizq, contder);
        }
    }
    
}
