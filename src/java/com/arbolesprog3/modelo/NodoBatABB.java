/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arbolesprog3.modelo;

import java.io.Serializable;

/**
 *
 * @author Admin
 */
public class NodoBatABB implements Serializable {
    
    private NodoBatABB izquierda;
    private NodoBatABB derecha;
    private TipoBarco barco;
    
    // GET Y SET

    public NodoBatABB getIzquierda() {
        return izquierda;
    }

    public void setIzquierda(NodoBatABB izquierda) {
        this.izquierda = izquierda;
    }

    public NodoBatABB getDerecha() {
        return derecha;
    }

    public void setDerecha(NodoBatABB derecha) {
        this.derecha = derecha;
    }

    public TipoBarco getBarco() {
        return barco;
    }

    public void setBarco(TipoBarco barco) {
        this.barco = barco;
    }

    
    //CONSTRUCTOR

    public NodoBatABB() {
    }

    public NodoBatABB(TipoBarco barco) {
        this.barco = barco;
    }

    public NodoBatABB(NodoBatABB izquierda, NodoBatABB derecha) {
        this.izquierda = izquierda;
        this.derecha = derecha;
    }

    
    
    
   
    
    //METODOS
    
    
    
    //TO STRING

    @Override
    public String toString() {
        return "NodoBatABB{" + "izquierda=" + izquierda + ", derecha=" + derecha + ", barco=" + barco + '}';
    }
    
    
}
