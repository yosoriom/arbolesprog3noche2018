/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arbolesprog3.modelo;

import java.io.Serializable;

/**
 *
 * @author Admin
 */
public class Rol implements Serializable {
    
    private byte codigo;
    private String nombre;

    public Rol(byte codigo, String nombre) {
        this.codigo = codigo;
        this.nombre = nombre;
    }

    public Rol() {
    }

    public byte getCodigo() {
        return codigo;
    }

    public void setCodigo(byte codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    
}
