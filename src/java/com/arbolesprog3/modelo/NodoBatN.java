/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arbolesprog3.modelo;

import java.util.ArrayList;

/**
 *
 * @author Admin
 */
public class NodoBatN {
    
    
    ArrayList<NodoBatN> hijos = new ArrayList<>();
    private TipoBarco barco;
    
    //CONSTRUCTOR

    public NodoBatN() {
    }

    public NodoBatN(TipoBarco barco) {
        this.barco = barco;
    }
    
    
    //GET Y SET

    public ArrayList<NodoBatN> getHijos() {
        return hijos;
    }

    public void setHijos(ArrayList<NodoBatN> hijos) {
        this.hijos = hijos;
    }

    public TipoBarco getBarco() {
        return barco;
    }

    public void setBarco(TipoBarco barco) {
        this.barco = barco;
    }
    
    
    //TO STRING

    @Override
    public String toString() {
        return "NodoBatN{" + "hijos=" + hijos + ", barco=" + barco + '}';
    }
    
}
