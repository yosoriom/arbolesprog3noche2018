/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arbolesprog3.modelo;

import java.io.Serializable;

/**
 *
 * @author carloaiza
 */
public class NodoABB implements Serializable {
    private Celular dato;
    private NodoABB izquierda;
    private NodoABB derecha;
    private TipoBarco barco;
    private int fe;

    public NodoABB() {
    }
    
    

    public NodoABB(TipoBarco barco) {
        this.barco = barco;
    }

    public NodoABB(Celular dato, NodoABB izquierda, NodoABB derecha, int fe) {
        this.dato = dato;
        this.izquierda = izquierda;
        this.derecha = derecha;
        this.fe = 0;
    }

    
    public int getFe() {
        return fe;
    }

    public void setFe(int fe) {
        this.fe = fe;
    }
    
    

    public TipoBarco getBarco() {
        return barco;
    }

    public void setBarco(TipoBarco barco) {
        this.barco = barco;
    }
    
    

    public NodoABB(Celular dato) {
        this.dato = dato;
    }

    public Celular getDato() {
        return dato;
    }

    public void setDato(Celular dato) {
        this.dato = dato;
    }

    public NodoABB getIzquierda() {
        return izquierda;
    }

    public void setIzquierda(NodoABB izquierda) {
        this.izquierda = izquierda;
    }

    public NodoABB getDerecha() {
        return derecha;
    }

    public void setDerecha(NodoABB derecha) {
        this.derecha = derecha;
    }
    
    
    public boolean esHoja()
    {
        return izquierda==null && derecha==null;
    }

    
    
    
    @Override
    public String toString() {
        return "NodoABB{" + "dato=" + dato + ", izquierda=" + izquierda + ", derecha=" + derecha + ", barco=" + barco + '}';
    }
}
