/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arbolesprog3.modelo;

/**
 *
 * @author Admin
 */
public class ArbolBatN {
    
    private NodoBatN raiz;
    private int cantidadNodos;
    
    //CONSTRUCTOR

    public ArbolBatN(NodoBatN raiz) {
        this.raiz = raiz;
    }

    public ArbolBatN() {
    }
    
    
    //GET Y SET

    public NodoBatN getRaiz() {
        return raiz;
    }

    public void setRaiz(NodoBatN raiz) {
        this.raiz = raiz;
    }

    public int getCantidadNodos() {
        return cantidadNodos;
    }

    public void setCantidadNodos(int cantidadNodos) {
        this.cantidadNodos = cantidadNodos;
    }
    
    

    
    //METODOS
    
    public void cargarBarcos (TipoBarco tbarco, String padre){
        
        for (int i = 0; Integer.parseInt(tbarco.getNroCasillas()) >= i; i++) {
            raiz.getHijos().add(new NodoBatN(tbarco));
        }
    }
    
    //TIPOS DE BARCOS
    
    public NodoBatN buscarPadrexIdBarco(String id){
        if (raiz != null)
        {
            return buscarPadrexIdBarco(id, raiz);
        }
        return null;
    }
    
    private NodoBatN buscarPadrexIdBarco(String id, NodoBatN pivote)
    {
        
        if(pivote.getBarco().getNroCasillas().compareTo(id)==0)
        {
            return pivote;
        }
        else
        {
            for (NodoBatN hijo : pivote.getHijos())             
            
            {
                NodoBatN padreEnc = buscarPadrexIdBarco(id, hijo);
                if (padreEnc != null)
                {
                    return padreEnc;
                }
            }
            return null;
        }
    }
    
    public void adicionarNodoNBarco (TipoBarco dato, String padre)
    {        
           
        if (raiz == null)
        {
            raiz = new NodoBatN(dato);
        }
        else
        {
            NodoBatN padreEnc = buscarPadrexIdBarco(padre);
            if (padreEnc != null)
            {
                padreEnc.getHijos().add(new NodoBatN(dato));
               
            }
        }
    }
    
    
    
    
    ///////////////////////////////////////////
    
    
    public void adicionarNodoxCodigo(TipoBarco dato, TipoBarco padre) 
    {
       if(raiz == null)
       {
           raiz = new NodoBatN(dato);
           
       }
       else
       {
           adicionarNodoxCodigo(dato, padre, raiz);
           
       }
       cantidadNodos++;
       
    }
    
    public boolean adicionarNodoxCodigo(TipoBarco dato, TipoBarco padre, NodoBatN pivote) 
    {
           // boolean adicionado=false;
          if(pivote.getBarco().getCodigo()==padre.getCodigo())
          {
              //Es el padre donde debo adicionar
              pivote.getHijos().add(new NodoBatN(dato));
              //adicionado=true;
              return true;
          }
          else
          {
              for(NodoBatN hijo: pivote.getHijos())
              {
                  if(adicionarNodoxCodigo(dato, padre,hijo))
                    break;
                  
              }
          }    
          return false;
    }
    
   
   
    
}
