/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arbolesprog3.modelo;

import com.arbolesprog3.excepcion.CelularExcepcion;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Admin
 */
public class ArbolBatABB implements Serializable{
    
    private NodoBatABB raiz;
    private int cantidadNodos;
    int altura;
    
    // GET Y SET

    public NodoBatABB getRaiz() {
        return raiz;
    }

    public void setRaiz(NodoBatABB raiz) {
        this.raiz = raiz;
    }

    
    public int getCantidadNodos() {
        return cantidadNodos;
    }

    public void setCantidadNodos(int cantidadNodos) {
        this.cantidadNodos = cantidadNodos;
    }

    public int getAltura() {
        return altura;
    }

    public void setAltura(int altura) {
        this.altura = altura;
    }
    
    //CONSTRUCTORES

    public ArbolBatABB() {
    }

    public ArbolBatABB(NodoBatABB raiz, int cantidadNodos, int altura) {
        this.raiz = raiz;
        this.cantidadNodos = cantidadNodos;
        this.altura = altura;
    }

    
    //-METODOS-
    
    // ES VACIO
    public boolean esVacio()
    {
        return raiz==null;
    }
    
    // ADICIONAR TIPO BARCO
    public void adicionarTipoBarco(TipoBarco bar) {
        NodoBatABB nuevo = new NodoBatABB(bar);
        if (raiz == null) {
            raiz = nuevo;
        } else {
            adicionarTipoBarco(nuevo,raiz);
        }
        
    }

    private void adicionarTipoBarco(NodoBatABB nuevo, NodoBatABB pivote)
            {
        if (nuevo.getBarco().getNroCasillas().compareTo(pivote.getBarco().getNroCasillas()) == 0) {
            
        } else if (nuevo.getBarco().getNroCasillas().compareTo(pivote.getBarco().getNroCasillas()) < 0) {
            //Va por la izq
            if (pivote.getIzquierda() == null) {
                pivote.setIzquierda(nuevo);
            } else {
                adicionarTipoBarco(nuevo, pivote.getIzquierda());
            }
        } else {
            //Va por la derecha
            if (pivote.getDerecha()== null) {
                pivote.setDerecha(nuevo);
            } else {
                adicionarTipoBarco(nuevo, pivote.getDerecha());
            }
        }
    }
    
    
    //RECORRERPREORDENTP
    public List<TipoBarco> recorrerPreOrdenTB() {
        List<TipoBarco> listaTiposBarcos=new ArrayList<>();
        recorrerPreOrdenTB(raiz,listaTiposBarcos);
        return listaTiposBarcos;
    }

    private void recorrerPreOrdenTB(NodoBatABB reco,List<TipoBarco> listadoB) {
        if (reco != null) {
            listadoB.add(reco.getBarco());
            recorrerPreOrdenTB(reco.getIzquierda(),listadoB);            
            recorrerPreOrdenTB(reco.getDerecha(),listadoB);
        }
    }
    
    
    public int sumarInOrdenRecursivoBarco(NodoBatABB reco) {
        int suma = 0;
        if (reco != null) {
            suma += sumarInOrdenRecursivoBarco(reco.getIzquierda());
            suma += Integer.parseInt(reco.getBarco().getCantidadJuego());
            suma += sumarInOrdenRecursivoBarco(reco.getDerecha());
        }
        return suma;

    }
    
    private int sumaCantidadB;
    public int sumarBarcos() {
         sumaCantidadB=0;         
        sumarInOrden(raiz);
        return sumaCantidadB;
    }
    private void sumarInOrden(NodoBatABB reco) {
        if (reco != null) {
            sumarInOrden(reco.getIzquierda());
            sumaCantidadB += Integer.parseInt(reco.getBarco().getCantidadJuego());
            sumarInOrden(reco.getDerecha());
        }
        
    }
}
