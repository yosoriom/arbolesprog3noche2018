/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arbolesprog3.modelo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Admin
 */
public class ArbolN implements Serializable{
    
    private NodoN raiz;

    public ArbolN() {
    }

    public NodoN getRaiz() {
        return raiz;
    }

    public void setRaiz(NodoN raiz) {
        this.raiz = raiz;
    }
    
    public NodoN buscarPadrexId (String id){
        if (raiz != null)
        {
            return buscarPadrexId(id, raiz);
        }
        return null;
    }
    
    private NodoN buscarPadrexId (String id, NodoN pivote)
    {
        if(pivote.getDato().getImei().compareTo(id)==0)
        {
            return pivote;
        }
        else
        {
            for (NodoN hijo : pivote.getHijos())             
            
            {
                NodoN padreEnc = buscarPadrexId(id, hijo);
                if (padreEnc != null)
                {
                    return padreEnc;
                }
            }
            return null;
        }
    }
    
    public void adicionarNodoN (Celular dato, String padre)
    {        
           
        if (raiz == null)
        {
            raiz = new NodoN(dato);
        }
        else
        {
            NodoN padreEnc = buscarPadrexId(padre);
            if (padreEnc != null)
            {
                padreEnc.getHijos().add(new NodoN(dato));
               
            }
        }
    }
    
    public void cargarBarcos (TipoBarco tbarco, String padre){
        
        for (int i = 0; Integer.parseInt(tbarco.getNroCasillas()) >= i; i++) {
            raiz.getHijos().add(new NodoN(tbarco));
        }
    }  
    
    
    //TIPOS DE BARCOS
    
    public NodoN buscarPadrexIdBarco(String id){
        if (raiz != null)
        {
            return buscarPadrexIdBarco(id, raiz);
        }
        return null;
    }
    
    private NodoN buscarPadrexIdBarco(String id, NodoN pivote)
    {
        
        if(pivote.getBarco().getNroCasillas().compareTo(id)==0)
        {
            return pivote;
        }
        else
        {
            for (NodoN hijo : pivote.getHijos())             
            
            {
                NodoN padreEnc = buscarPadrexIdBarco(id, hijo);
                if (padreEnc != null)
                {
                    return padreEnc;
                }
            }
            return null;
        }
    }
    
    public void adicionarNodoNBarco (TipoBarco dato, String padre)
    {        
           
        if (raiz == null)
        {
            raiz = new NodoN(dato);
        }
        else
        {
            NodoN padreEnc = buscarPadrexIdBarco(padre);
            if (padreEnc != null)
            {
                padreEnc.getHijos().add(new NodoN(dato));
               
            }
        }
    }
    
   
    
}
