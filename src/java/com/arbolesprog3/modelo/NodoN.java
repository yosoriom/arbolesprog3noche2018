/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arbolesprog3.modelo;

import java.io.Serializable;
import java.util.List;
import java.util.ArrayList;

/**
 *
 * @author Admin
 */
public class NodoN implements Serializable{
    
    private Celular dato;
    ArrayList<NodoN> hijos = new ArrayList<>();
    private TipoBarco barco;

    public NodoN(TipoBarco barco) {
        this.barco = barco;
    }
    
    
    public NodoN() {
    }

    public NodoN(Celular dato) {
        this.dato = dato;
    }

    public TipoBarco getBarco() {
        return barco;
    }

    public void setBarco(TipoBarco barco) {
        this.barco = barco;
    }
    
    

    public Celular getDato() {
        return dato;
    }

    public void setDato(Celular dato) {
        this.dato = dato;
    }

    public ArrayList<NodoN> getHijos() {
        return hijos;
    }

    public void setHijos(ArrayList<NodoN> hijos) {
        this.hijos = hijos;
    }

   

    @Override
    public String toString() {
        return "NodoN{" + "barco=" + barco + ", hijos=" + hijos + '}';
    }
   
}

