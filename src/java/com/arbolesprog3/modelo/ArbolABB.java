/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arbolesprog3.modelo;

import com.arbolesprog3.excepcion.CelularExcepcion;
import com.arbolesprog3.validador.CelularValidador;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author carloaiza
 */
public class ArbolABB implements Serializable {

    private NodoABB raiz;
    private int cantidadNodos;
    int altura;
 
    public ArbolABB() {
    }

    public NodoABB getRaiz() {
        return raiz;
    }

    public void setRaiz(NodoABB raiz) {
        this.raiz = raiz;
    }

    public int getCantidadNodos() {
        return cantidadNodos;
    }

    public void setCantidadNodos(int cantidadNodos) {
        this.cantidadNodos = cantidadNodos;
    }

    ///Adicionar en el árbol
    public void adicionarNodo(Celular dato) throws CelularExcepcion {

        CelularValidador.validarDatos(dato);
        NodoABB nuevo = new NodoABB(dato);
        if (raiz == null) {
            raiz = nuevo;
        } else {
            adicionarNodo(nuevo,raiz);
        }
        cantidadNodos++;
    }

    private void adicionarNodo(NodoABB nuevo, NodoABB pivote)
            throws CelularExcepcion {
        if (nuevo.getDato().getImei().compareTo(pivote.getDato().getImei()) == 0) {
            throw new CelularExcepcion("Ya existe un celular con el imei "
                    + nuevo.getDato().getImei());
        } else if (nuevo.getDato().getImei().compareTo(pivote.getDato().
                getImei()) < 0) {
            //Va por la izq
            if (pivote.getIzquierda() == null) {
                pivote.setIzquierda(nuevo);
            } else {
                adicionarNodo(nuevo, pivote.getIzquierda());
            }
        } else {
            //Va por la derecha
            if (pivote.getDerecha()== null) {
                pivote.setDerecha(nuevo);
            } else {
                adicionarNodo(nuevo, pivote.getDerecha());
            }
        }
    }
    
    
    public List<Celular> recorrerOperadores(String operador) {
        List<Celular> listaCelulares=new ArrayList<>();
        recorrerOperadores(raiz,listaCelulares,operador);
        return listaCelulares;
    }

    private void recorrerOperadores(NodoABB reco,List<Celular> listado, String operador) {
        if (reco != null) {
            if (reco.getDato().getOperador().getNombre().compareTo(operador)==0){
                listado.add(reco.getDato());                    
            }
            recorrerOperadores(reco.getIzquierda(),listado, operador);                
            recorrerOperadores(reco.getDerecha(),listado, operador);
        }
    }
    
     public List<Celular> recorrerInOrden() {
        List<Celular> listaCelulares=new ArrayList<>();
        recorrerInOrden(raiz,listaCelulares);
        return listaCelulares;
    }

    private void recorrerInOrden(NodoABB reco,List<Celular> listado) {
        if (reco != null) {
            recorrerInOrden(reco.getIzquierda(),listado);
            listado.add(reco.getDato());
            recorrerInOrden(reco.getDerecha(),listado);
        }
    }

     public List<Celular> recorrerPreOrden() {
        List<Celular> listaCelulares=new ArrayList<>();
        recorrerPreOrden(raiz,listaCelulares);
        return listaCelulares;
    }

    private void recorrerPreOrden(NodoABB reco,List<Celular> listado) {
        if (reco != null) {
            listado.add(reco.getDato());
            recorrerPreOrden(reco.getIzquierda(),listado);            
            recorrerPreOrden(reco.getDerecha(),listado);
        }
    }
    
     public List<Celular> recorrerPostOrden() {
        List<Celular> listaCelulares=new ArrayList<>();
        recorrerPostOrden(raiz,listaCelulares);
        return listaCelulares;
    }

    private void recorrerPostOrden(NodoABB reco,List<Celular> listado) {
        if (reco != null) {
            
            recorrerPostOrden(reco.getIzquierda(),listado);            
            recorrerPostOrden(reco.getDerecha(),listado);
            listado.add(reco.getDato());
        }
    }
    
    public boolean esVacio()
    {
        return raiz==null;
    }
    
   
    private double sumaPrecios;
    
    
     public double sumarPrecios() {
         sumaPrecios=0;         
        sumarInOrden(raiz);
        return sumaPrecios;
    }
    private void sumarInOrden(NodoABB reco) {
        if (reco != null) {
            sumarInOrden(reco.getIzquierda());
            sumaPrecios += reco.getDato().getPrecio();
            sumarInOrden(reco.getDerecha());
        }
        
    }
    public double calcularPromedioPrecios()
    {
        return  sumarPrecios()/(double) cantidadNodos;
    }
    
    //Borrar el que sea
    public String borrarImei(String eli) {      

        NodoABB z = borrarImei(this.raiz, eli);
        this.setRaiz(z);
        return eli;

    }

    private NodoABB borrarImei (NodoABB reco, String eli) {
        if (reco == null) {
            return null;//<--Dato no encontrado		
        }
        int compara = (reco.getDato().getImei()).compareTo(eli);
        if (compara > 0) {
            reco.setIzquierda(borrarImei(reco.getIzquierda(), eli));
        } else if (compara < 0) {
            reco.setDerecha(borrarImei(reco.getDerecha(), eli));
        } else {
            
            if (reco.getIzquierda()!= null && reco.getDerecha()!= null) {
                /*
                 *	Buscar el menor de los derechos y lo intercambia por el dato
                 *	que desea borrar. La idea del algoritmo es que el dato a borrar 
                 *	se coloque en una hoja o en un nodo que no tenga una de sus ramas.
                 **/
                NodoABB cambiar = buscarMinimo(reco.getDerecha());
                Celular aux = cambiar.getDato();
                cambiar.setDato(reco.getDato());
                reco.setDato(aux);
                reco.setDerecha(borrarImei(reco.getDerecha(), eli));
                
            } else {
                reco = (reco.getIzquierda()!= null) ? reco.getIzquierda(): reco.getDerecha();
                
            }
        }
        return reco;
    }
    
    public NodoABB buscarMinimo(NodoABB reco) {
        for (; reco.getIzquierda()!= null; reco = reco.getIzquierda());
        return (reco);
    }
    
    protected boolean esHoja(NodoABB reco) {
        return (reco != null && reco.getIzquierda()== null && reco.getDerecha()== null);
    }
    
    
    public void podar() {
        podar(this.raiz);
    }

    private void podar(NodoABB reco) {
        if (reco == null) {
            return;
        }
        
        if (this.esHoja(reco.getIzquierda())) {
            reco.setIzquierda(null);
        }
        if (this.esHoja(reco.getDerecha())) {
            reco.setDerecha(null);
        }
        
        podar(reco.getIzquierda());
        podar(reco.getDerecha());
    }
    
    /////////////////7
    ///////////////777
    //////////////////7
    //////////////////7777777777
    
    
    public void adicionarTipoBarco(TipoBarco bar) throws CelularExcepcion {

        
        NodoABB nuevo = new NodoABB(bar);
        if (raiz == null) {
            raiz = nuevo;
        } else {
            adicionarTipoBarco(nuevo,raiz);
        }
        
    }

    private void adicionarTipoBarco(NodoABB nuevo, NodoABB pivote)
            throws CelularExcepcion {
        if (nuevo.getBarco().getNroCasillas().compareTo(pivote.getBarco().getNroCasillas()) == 0) {
            throw new CelularExcepcion("Ya existe un barco con el mismo numero de casillas "
                    + nuevo.getBarco().getNroCasillas());
        } else if (nuevo.getBarco().getNroCasillas().compareTo(pivote.getBarco().getNroCasillas()) < 0) {
            //Va por la izq
            if (pivote.getIzquierda() == null) {
                pivote.setIzquierda(nuevo);
            } else {
                adicionarTipoBarco(nuevo, pivote.getIzquierda());
            }
        } else {
            //Va por la derecha
            if (pivote.getDerecha()== null) {
                pivote.setDerecha(nuevo);
            } else {
                adicionarTipoBarco(nuevo, pivote.getDerecha());
            }
        }
    }
    
    public List<TipoBarco> recorrerPreOrdenTB() {
        List<TipoBarco> listaTiposBarcos=new ArrayList<>();
        recorrerPreOrdenTB(raiz,listaTiposBarcos);
        return listaTiposBarcos;
    }

    private void recorrerPreOrdenTB(NodoABB reco,List<TipoBarco> listadoB) {
        if (reco != null) {
            listadoB.add(reco.getBarco());
            recorrerPreOrdenTB(reco.getIzquierda(),listadoB);            
            recorrerPreOrdenTB(reco.getDerecha(),listadoB);
        }
    }
    
    String[] niveles;

    public int alturaArbol() {        
        alturaArbol(raiz, 0);
        return altura;
    }

    private void alturaArbol(NodoABB pivote, int nivel) {
        
        if (pivote != null) {
            alturaArbol(pivote.getIzquierda(), nivel + 1);
            alturaArbol(pivote.getDerecha(), nivel + 1);
        if (nivel > altura) {
            altura = nivel;
            }
        alturaArbol(pivote.getIzquierda(), nivel + 1);
        alturaArbol(pivote.getDerecha(), nivel + 1);
        }
    }

    public ArrayList imprimirNivel() {
        niveles = new String[altura + 1];
        ArrayList tb=new ArrayList();
        imprimirNivel(raiz, 0);
        for (String nivele : niveles) {
            tb.add(nivele + " ");
            //System.out.println(niveles[i] + " ");
        }
        return tb;
    }
      public void imprimirNivel(NodoABB pivote, int nivel2) {
        if (pivote != null) {
            niveles[nivel2] = pivote.getBarco() + ", " + ((niveles[nivel2] != null) ? niveles[nivel2] : "");
            imprimirNivel(pivote.getDerecha(), nivel2 + 1);
            imprimirNivel(pivote.getIzquierda(), nivel2 + 1);
        }
    }
      
    public ArrayList imprimirNivelImei() {
        niveles = new String[altura + 1];
        ArrayList tb=new ArrayList();
        imprimirNivelImei(raiz, 0);
        for (String nivele : niveles) {
            tb.add(nivele + " ");
            //System.out.println(niveles[i] + " ");
        }
        return tb;
    }
    public void imprimirNivelImei(NodoABB pivote, int nivel2) {
        if (pivote != null) {
            niveles[nivel2] = pivote.getDato() + ", " + ((niveles[nivel2] != null) ? niveles[nivel2] : "");
            imprimirNivelImei(pivote.getDerecha(), nivel2 + 1);
            imprimirNivelImei(pivote.getIzquierda(), nivel2 + 1);
        }
    }
    
    //hojas
    public ArrayList cantidadHojas() {
        ArrayList hoja = new ArrayList();
        cantidadHojas(this.raiz, hoja);
        return (hoja);
    }

    private void cantidadHojas(NodoABB reco, ArrayList hoja) {
        if (reco != null) {
            if (this.esHoja(reco)) {
                hoja.add(reco.getDato());
            }
            cantidadHojas(reco.getIzquierda(), hoja);
            cantidadHojas(reco.getDerecha(), hoja);
        }

    }
    
    //Valor menor
     public String menorValor() {
         NodoABB reco = raiz;
        if (raiz != null) {
            
            while (reco.getIzquierda()!= null) {
                reco = reco.getIzquierda();
            }
        }
        return ("" + reco.getDato());
    }
     //mayor Valor
    public String mayorValor() {
        NodoABB reco = raiz;
        if (raiz != null) {
            while (reco.getDerecha()!= null) {
                reco = reco.getDerecha();
            }
        }
        return ("" + reco.getDato());
    }
    
    public NodoABB buscar(String d, NodoABB reco){
        if(raiz==null){
            return null;
        }else if(reco.getDato().getImei().compareTo(d) == 0){
            return reco;
        }else if(d.compareTo(reco.getDato().getImei()) < 0){
            return buscar(d, reco.getDerecha());
        }else{
            return buscar(d, reco.getIzquierda());
        }
    }
    
    //factor de equilibrio
    
    public int obtenerFE(NodoABB reco){
        if(reco==null){
            return -1;
        }else{
            return reco.getFe();
        }
    }
    
    //rotacion simple izquierda
    
    public NodoABB rotacionIzq(NodoABB reco){
        NodoABB auxiliar;
        auxiliar = reco.getIzquierda();
        reco.setIzquierda(auxiliar.getDerecha());
        auxiliar.setDerecha(reco);
        reco.setFe(Math.max(obtenerFE(reco.getIzquierda()), obtenerFE(reco.getDerecha()))+1);
        auxiliar.setFe(Math.max(obtenerFE(auxiliar.getIzquierda()), obtenerFE(auxiliar.getDerecha()))+1);
        return auxiliar;                
    }
    
    //rotacion simple derecha
    
    public NodoABB rotacionDer(NodoABB reco){
        NodoABB auxiliar;
        auxiliar = reco.getDerecha();
        reco.setDerecha(auxiliar.getIzquierda());
        auxiliar.setIzquierda(reco);
        reco.setFe(Math.max(obtenerFE(reco.getIzquierda()), obtenerFE(reco.getDerecha()))+1);
        auxiliar.setFe(Math.max(obtenerFE(auxiliar.getIzquierda()), obtenerFE(auxiliar.getDerecha()))+1);
        return auxiliar;                
    }
    
    //rotacion doble a la derecha
    
    public NodoABB rotacionDobleIzq(NodoABB reco){
        NodoABB temporal;
        reco.setIzquierda(rotacionDer(reco.getIzquierda()));
        temporal=rotacionIzq(reco);
        return temporal;
    }
    
    //rotacion doble ozquierda
    
    public NodoABB rotacionDobleDer(NodoABB reco){
        NodoABB temporal;
         reco.setDerecha(rotacionIzq(reco.getDerecha()));
         temporal=rotacionDer(reco);
         return temporal;                 
    }
    
    //metodo insertar AVL
    
    public NodoABB insertarAVL(NodoABB nuevo, NodoABB subar){
        NodoABB nuevoPadre = subar;
        if(nuevo.getDato().getImei().compareTo(subar.getDato().getImei()) <0){
            if(subar.getIzquierda()==null){
                subar.setIzquierda(nuevo);
            }else{
                subar.setIzquierda(insertarAVL(nuevo, subar.getIzquierda()));
                if((obtenerFE(subar.getIzquierda()) - (obtenerFE(subar.getDerecha()))) == 2){
                    if(nuevo.getDato().getImei().compareTo(subar.getIzquierda().getDato().getImei())<0){
                        nuevoPadre = rotacionIzq(subar);
                    }else{
                        nuevoPadre = rotacionDobleIzq(subar);
                    }
                }
            }
        }else if(nuevo.getDato().getImei().compareTo(subar.getDato().getImei())  >0){
            if(subar.getDerecha()==null){
                subar.setDerecha(nuevo);
            }else{
                subar.setDerecha(insertarAVL(nuevo, subar.getDerecha()));
                if((obtenerFE(subar.getDerecha()) - (obtenerFE(subar.getIzquierda()))) == 2){
                    if(nuevo.getDato().getImei().compareTo(subar.getDerecha().getDato().getImei()) > 0){
                        nuevoPadre=rotacionDer(subar);
                    }else{
                        nuevoPadre=rotacionDobleDer(subar);
                    }
                }
            }
        }else{
            System.out.println("nodo duplicado");
        }
        //Actualizando el FE
        if((subar.getIzquierda()==null) && (subar.getDerecha()!=null)){
            subar.setFe(subar.getDerecha().getFe()+1);
        }else if((subar.getDerecha()==null) && (subar.getIzquierda() != null)){
            subar.setFe(subar.getIzquierda().getFe()+1);
        }else{
            subar.setFe(Math.max(obtenerFE(subar.getIzquierda()), obtenerFE(subar.getDerecha()))+1);
            
        }
        return nuevoPadre;
    }
    
    //metodo para insertar
    public void insertar(Celular dato){
        NodoABB nuevo = new NodoABB(dato);
        if(raiz==null){
            raiz=nuevo;
        }else{
            raiz = insertarAVL(nuevo, raiz);
        }
    }
    
    public int sumarInOrdenRecursivoBarco(NodoABB reco) {
        int suma = 0;
        if (reco != null) {
            suma += sumarInOrdenRecursivoBarco(reco.getIzquierda());
            suma += Integer.parseInt(reco.getBarco().getNroCasillas());
            suma += sumarInOrdenRecursivoBarco(reco.getDerecha());
        }
        return suma;

    }
    
    
}

