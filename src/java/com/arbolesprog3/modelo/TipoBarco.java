/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arbolesprog3.modelo;

import java.io.Serializable;

/**
 *
 * @author Admin
 */
public class TipoBarco implements Serializable{
    
    private String nombre;
    private String nroCasillas;
    private String cantidadJuego;

    private int codigo;

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }
    
    

    public TipoBarco() {
    }

    public TipoBarco(String nombre, String nroCasillas, String cantidadJuego) {
        this.nombre = nombre;
        this.nroCasillas = nroCasillas;
        this.cantidadJuego = cantidadJuego;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNroCasillas() {
        return nroCasillas;
    }

    public void setNroCasillas(String nroCasillas) {
        this.nroCasillas = nroCasillas;
    }

    public String getCantidadJuego() {
        return cantidadJuego;
    }

    public void setCantidadJuego(String cantidadJuego) {
        this.cantidadJuego = cantidadJuego;
    }

    @Override
    public String toString() {
        return nombre + " " + codigo ;
        
    }

    

    
    
}
